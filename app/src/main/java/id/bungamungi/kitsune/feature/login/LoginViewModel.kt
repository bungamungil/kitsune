package id.bungamungi.kitsune.feature.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import id.bungamungi.kitsune.model.form.LoginForm
import id.bungamungi.kitsune.network.KitsuService

class LoginViewModel : ViewModel() {

    val progress by lazy { MutableLiveData<Boolean>() }

    val error by lazy { MutableLiveData<String>() }

    fun login(loginForm: LoginForm) {
        progress.value = true
        KitsuService.login(loginForm) {
            progress.value = false
            it.subscribe({
                Log.d("success", it.accessToken)
            }, {
                error.value = "Ups, ${it.localizedMessage}"
            })
        }
    }

}