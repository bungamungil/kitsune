package id.bungamungi.kitsune.feature.error

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.bungamungi.kitsune.R
import kotlinx.android.synthetic.main.error.view.*

class ErrorDialogFragment : BottomSheetDialogFragment() {

    private var message: String = ""

    fun setMessage(message: String) : ErrorDialogFragment {
        this.message = message
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.error, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.errorMessageLabel.text = message
    }

}