package id.bungamungi.kitsune.feature.login

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.Toast
import id.bungamungi.kitsune.R
import id.bungamungi.kitsune.base.BaseActivity
import id.bungamungi.kitsune.model.form.LoginForm
import kotlinx.android.synthetic.main.login.*

class LoginActivity : BaseActivity(), LoginForm {

    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    private val dimDialog by lazy {
        Dialog(this).also {
            it.setCancelable(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        setupLoginButton()
        setupProgressObserver()
        setupErrorObserver()
    }

    private fun setupLoginButton() {
        loginButton.setOnClickListener {
            viewModel.login(this)
        }
    }

    private fun setupProgressObserver() {
        viewModel.progress.observe(this, Observer {
            if (it == true) {
                dimDialog.show()
                loginProgressBar.visibility = View.VISIBLE
            } else {
                dimDialog.dismiss()
                loginProgressBar.visibility = View.INVISIBLE
            }
        })
    }

    private fun setupErrorObserver() {
        viewModel.error.observe(this, Observer {
            val error = it ?: return@Observer
//            showErrorDialog(error)
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        })
    }

    override fun username(): String {
        return inputUsername.text.toString()
    }

    override fun password(): String {
        return inputPassword.text.toString()
    }

}
