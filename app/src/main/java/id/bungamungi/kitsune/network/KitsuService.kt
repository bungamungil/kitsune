package id.bungamungi.kitsune.network

import com.github.kittinunf.fuel.httpPost
import com.squareup.moshi.Moshi
import id.bungamungi.kitsune.model.form.LoginForm
import id.bungamungi.kitsune.model.mutable.MutableOauthToken
import id.bungamungi.kitsune.model.immutable.OauthToken
import io.reactivex.Observable

object KitsuService {

    private const val baseUrl = "https://kitsu.io/api/edge"

    private const val authUrl = "https://kitsu.io/api"

    private const val clientId = "dd031b32d2f56c990b1425efe6c42ad847e7fe3ab46bf1299f05ecd856bdb7dd"

    private const val clientSecret = "54d7307928f63414defd96399fc31ba847961ceaecef3a5fd93144e960c0e151"

    fun login(loginForm: LoginForm, then: (result: Observable<OauthToken>) -> Unit) {
        val url = "$authUrl/oauth/token"
        val parameters = listOf(
                Pair("grant_type", "password"),
                Pair("username", loginForm.username()),
                Pair("password", loginForm.password()),
                Pair("client_id", clientId),
                Pair("client_secret", clientSecret)
        )
        url.httpPost(parameters)
                .responseString { request, response, result ->
                    try {
                        if (response.statusCode == 200) {
                            val string = result.component1()
                                    ?: throw Exception("Please check if you are connected to Internet.")
                            val token = Moshi.Builder().build().adapter(MutableOauthToken::class.java).fromJson(string)?.toImmutable()
                                    ?: throw Exception("It was really hard for us to read your authentication ticket.")
                            then(Observable.just(token))
                        } else {
                            throw Exception(response.responseMessage)
                        }
                    } catch (exception: Throwable) {
                        then(Observable.error(exception))
                    }
                }
    }

}