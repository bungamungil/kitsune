package id.bungamungi.kitsune.base

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import id.bungamungi.kitsune.feature.error.ErrorDialogFragment

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private val errorDialogFragment by lazy { ErrorDialogFragment() }

    fun showErrorDialog(message: String) {
        errorDialogFragment
                .setMessage(message)
                .show(supportFragmentManager, "error-dialog-fragment")
    }

}