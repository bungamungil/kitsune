package id.bungamungi.kitsune.model.immutable

data class OauthToken(

        val accessToken: String,

        val tokenType: String,

        val expiresIn: Int,

        val refreshToken: String,

        val scope: String,

        val createdAt: Int

)