package id.bungamungi.kitsune.model.form

interface LoginForm {

    fun username() : String

    fun password() : String

}