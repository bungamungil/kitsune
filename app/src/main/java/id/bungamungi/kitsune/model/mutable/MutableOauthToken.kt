package id.bungamungi.kitsune.model.mutable

import com.squareup.moshi.Json
import id.bungamungi.kitsune.core.MutableResource
import id.bungamungi.kitsune.model.immutable.OauthToken

class MutableOauthToken : MutableResource<OauthToken> {

    @field:Json(name = "access_token")
    var accessToken: String? = null

    @field:Json(name = "token_type")
    var tokenType: String? = null

    @field:Json(name = "expires_in")
    var expiresIn: Int? = null

    @field:Json(name = "refresh_token")
    var refreshToken: String? = null

    @field:Json(name = "scope")
    var scope: String? = null

    @field:Json(name = "created_at")
    var createdAt: Int? = null

    override fun toImmutable(): OauthToken {
        val accessToken = accessToken ?: throw Exception("OauthToken doesn't have access token.")
        val tokenType = tokenType ?: throw Exception("OauthToken doesn't have token type.")
        val expiresIn = expiresIn ?: throw Exception("OauthToken doesn't have expires in.")
        val refreshToken = refreshToken ?: throw Exception("OauthToken doesn't have refresh token.")
        val scope = scope ?: throw Exception("OauthToken doesn't have scope.")
        val createdAt = createdAt ?: throw Exception("OauthToken doesn't have created at.")
        return OauthToken(accessToken, tokenType, expiresIn, refreshToken, scope, createdAt)
    }
}
