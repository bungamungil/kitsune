package id.bungamungi.kitsune.core

interface MutableResource<T> {

    fun toImmutable(): T

}